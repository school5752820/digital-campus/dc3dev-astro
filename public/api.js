const response = await fetch('https://dummyjson.com/posts');

if (!response.ok) {
  throw Error('HTTP status not expected');
}

const json = await response.json();
console.log(json)

// ==========================================

fetch('https://dummyjson.com/posts')
  .then((response) => {
    if (response.ok) {
      return response.json();
    }

    throw Error('HTTP status not expected');
  })
  .then((json) => {
    console.log(json);
  })